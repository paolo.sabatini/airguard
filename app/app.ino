/*
 sketch che monitora la qualità dell'aria ...
ciao alla 4a
*/
 

// include delle librerie 
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Adafruit_CCS811.h>


// dichiarazione della variabile ccs che memorizza il "sensore" :
Adafruit_CCS811 ccs;

/* questa riga si spiega così: 
      è simile ad una riga tipo    int num;   
      quindi ccs è una variabile di tipo  Adafruit_CCS811 
      non è di un tipo primitivo ( tipo int float ecc....)
      ma è una variabile OGGETTO (studierete più avanti in informatica )
      in questi casi invece di dire "di tipo Adafruit_CCS811" meglio dire "di classe Adafruit_CCS811 "
*/
// gli OGGETTI sono "cose" atte a rappresentare qualcosa in un codice (es. un sensore ... o vedi sotto un display ) 


// dichiarazione e inizializzazione dell'oggetto (ora uso propriamente la parola oggetto )
Adafruit_SSD1306 display = Adafruit_SSD1306();

/* questa riga si spiega così: 
       è simile ad una riga tipo   int num = 3 ;

      TO DO: continuare 
*/



void setup() {  

  Serial.begin(9600);
 
  //inizializzazione del sensore ...

  if(!ccs.begin()){  
    Serial.println("Failed to start sensor! Please check your wiring.");
    while(1);
  }

  /* le righe sopra si spiegano così :
       ccs è una variabile oggetto per brevità diremo è un oggetto;
       un oggetto ha PROPRIETÀ  e METODI .. (studierete più avanti )
       i METODI sono funzioni che appartengono all'oggetto 
       si richiamano così : oggetto.metodo( parametri )
       in questo caso abbiamo chiamato   ccs.begin()   (non ha parametri ...) 
       dal nome e dal resto si capisce che serve a inizializzare il sensore ...
       dal uso si capisce che il metodo begin() restituisce un boolean ... 
       l' if si legge quindi così
       se... non (!) inizia il sensore .... allora ...
  */
    
 
  // by default, we'll generate the high voltage from the 3.3v line internally! (neat!)
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3C (for the 128x32)

    
  // Clear the buffer.
  display.clearDisplay();
  display.display();


  
  Serial.println("IO test");

  // text display tests
  display.setTextSize(2);
  display.setTextColor(WHITE);
 
}


void loop() {
  
  
  if(ccs.available()){
   
    

    if(!ccs.readData()){
       unsigned int  eCO2 = ccs.geteCO2();
       unsigned int  TVOC = ccs.getTVOC();
      
      Serial.print("CO2: ");Serial.println(eCO2);
      Serial.print("VOC: ");Serial.println(TVOC);
      Serial.println();
      
      display.clearDisplay(); display.setCursor(0,0);
      display.print("CO2: ");display.println(eCO2);
      display.print("VOC: ");display.println(TVOC);
      display.display();
     

    }
    else{
      Serial.println("ERROR!");
      display.print("ERROR ");display.display();
      while(1);
    }
  }
  delay(500);
}
